import { initCommitBoxInfo } from '~/projects/commit_box/info';
import initPipelines from '~/commit/pipelines/pipelines_bundle';
import initCommitActions from '~/projects/commit';

initCommitBoxInfo();
initPipelines();
initCommitActions();
